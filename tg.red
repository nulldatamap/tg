Red [
    Title "Termination Game"
    needs 'view
]

ARENA-WIDTH:  4096

Encoding: context [
    opcodes: context [
        data: 0
        move: 1
        add: 2
        sub: 3
        mul: 4
        div: 5
        mod: 6
        jump: 7
        zjump: 8
        nzjump: 9
        eqskip: 10
        neskip: 11
        ltskip: 12
        decjump: 13
    ]

    op-mask:   0Fh
    flag-mask: 0Fh
    param-mask: 0FFFh
    op-shift:   28
    flag-shift: 24
    src-shift:  12
    dst-shift:   0
    src-ind-flag: 1
    dst-ind-flag: 2

    decode: func [s m] [ func [i] compose [ i >> (s) and (m) ] ]
    decode-op: decode op-shift op-mask
    decode-flags: decode flag-shift flag-mask
    decode-src: decode src-shift param-mask
    decode-dst: decode dst-shift param-mask
    decode-src-flag: func [f] [ f and src-ind-flag <> 0 ]
    decode-dst-flag: func [f] [ f and dst-ind-flag <> 0 ]

    encode: func [op sf df src dst] [
        (opcodes/:op << op-shift)
        or ((either sf [src-ind-flag][0]) << flag-shift)
        or ((either df [dst-ind-flag][0]) << flag-shift)
        or (src << src-shift)
        or (dst << dst-shift)
    ]
]

Simulator: context [
    make-simulator: function [] [
        object [
            width: ARENA-WIDTH
            cycle: 0
            memory: make vector! width 0
            pc-a: 0
            pc-b: 0
            program-a: none
            program-b: none
            status: 'tie
        ]
    ]

    init: function [sim a b] [
        sim/program-a: a
        sim/program-b: b
        a-len: length? a/code
        b-len: length? b/code

        ;; Pick two random non-overlapping starting positions
        forever [
            a-start: random ARENA-WIDTH
            a-end: a-start + a-len
            b-start: random ARENA-WIDTH
            b-end: b-start + b-len

            unless all [ a-start <= b-end
                         b-start <= a-end ] [
                break
            ]

        ]

        sim/pc-a: a-start - 1 + a/origin % sim/width
        sim/pc-b: b-start - 1 + b/origin % sim/width

        ;; Copy the programs' code
        repeat i a-len [
            sim/memory/(sim/pc-a + i % sim/width): a/code/:i
        ]
        repeat i b-len [
            sim/memory/(sim/pc-b + i % sim/width): b/code/:i
        ]
    ]

    step: function [sim] [
        norm-addr: function [addr][ wrap (current-pc + addr + 1) ]
        gets: function [addr][
            sim/memory/(norm-addr addr)
        ]
        sets: function [addr val][
            sim/memory/(norm-addr addr): val
        ]
        wrap: function [val][
            val: val % sim/width
            if val < 0 [
                val: sim/width + val
            ]
            val
        ]

        current-program: either (sim/cycle % 2) = 0 [ 'a ] [ 'b ]

        current-pc: either current-program = 'a [
            sim/pc-a
        ] [
            sim/pc-b
        ]

        instr: sim/memory/(current-pc + 1)
        op: Encoding/decode-op instr
        flags: Encoding/decode-flags instr
        src: Encoding/decode-src instr
        dst: Encoding/decode-dst instr

        either Encoding/decode-src-flag flags [
            src-val: gets src
        ] [
            src-val: src
        ]

        either Encoding/decode-dst-flag flags [
            dst: gets dst
            dst-val: gets dst
        ] [
            dst-val: gets dst
        ]

        dead: false
        ops: Encoding/opcodes
        switch/default op reduce [
            ops/move [ sets dst src-val ]
            ops/add [ sets dst (wrap (src-val + dst-val)) ]
            ops/sub [ sets dst (wrap (src-val - dst-val)) ]
            ops/mul [ sets dst (wrap (src-val * dst-val)) ]
            ops/div [ either src-val <> 0 [ sets dst (wrap (dst-val / src-val)) ] [ dead: true ] ]
            ops/mod [ either src-val <> 0 [ sets dst (wrap (dst-val % src-val)) ] [ dead: true ] ]
            ops/jump [ current-pc: current-pc - 1 + src-val ]
            ops/zjump [ if dst-val = 0 [ current-pc: current-pc - 1 + src-val ] ]
            ops/nzjump [ if dst-val <> 0 [ current-pc: current-pc - 1 + src-val ] ]
            ops/eqskip [ if dst-val = src-val [ current-pc: current-pc + 1 ] ]
            ops/neskip [ if dst-val <> src-val [ current-pc: current-pc + 1 ] ]
            ops/ltskip [ if src-val < dst-val [ current-pc: current-pc + 1 ] ]
            ops/decjump [ either dst-val = 0 [ current-pc: current-pc - 1 + src-val ] [ sets dst (wrap (dst-val - 1)) ] ]
        ] [
            dead: true
        ]

        if dead [
            either current-program = 'a [
                sim/status: 'b-wins
            ] [
                sim/status: 'a-wins
            ]
        ]

        current-pc: (current-pc + 1) % sim/width
        sim/cycle: sim/cycle + 1

        either current-program = 'a [
            sim/pc-a: current-pc
        ] [
            sim/pc-b: current-pc
        ]
    ]
]

Assembler: context [
    binary-instrs: [move add sub mul div mod zjump nzjump eqskip neskip ltskip decjump]
    unary-instrs: [data jump]
    supported-instrs: append copy binary-instrs unary-instrs

    parse-assembly: function [src] [
        name: "<unnamed>"
        version: none
        entrypoint: none
        instructions: copy []
        current-instruction: copy []
        opcode: none

        parse-immidiate: [
            integer! | word!
        ]

        parse-memory: [
            ahead block! into parse-immidiate
        ]

        parse-indirect: [
            ahead block! into parse-memory
        ]

        parse-param: [
            parse-immidiate | parse-memory | parse-indirect
        ]

        tmp: none
        parse-opcode: [
            set tmp word!
            if (find supported-instrs tmp) none
        ]

        parse-instruction: [
            collect set current-instruction [
                keep [set opcode parse-opcode]
                any [
                    not parse-opcode
                    keep parse-param
                ]
            ]
            keep (current-instruction)
        ]

        succ: parse src [
            any [ ['name set name string!]
                | ['version set version tuple!]
                | ['entrypoint set entrypoint [word! | integer!]]
            ]

            collect into instructions [ any [
                keep set-word! | parse-instruction
            ]]
        ]


        either succ [
            object compose/only [
                name: (name)
                version: (version)
                entrypoint: quote (entrypoint)
                instructions: (instructions)
                code: none
                origin: 0
            ]
        ] [
            none
        ]
    ]

    assemble-block: function [src] [
        program: parse-assembly src

        if not program [
            throw [parse-error]
        ]

        ;; Validate
        foreach instr program/instructions [
            if set-word? instr [ continue ]
            opcode: first instr
            params: next instr

            arity: either find binary-instrs opcode [ 2 ] [ 1 ]
            if (length? params) <> arity [
                throw reduce ['arity opcode]
            ]

            ;; Indirect src param
            either block? params/1 [
                if block? params/1/1 [
                    throw reduce ['indirect-src opcode]
                ]
                params/1: reduce ['mem params/1/1]
            ] [
                params/1: reduce ['imm params/1]
            ]

            ;; TODO: What about conditional jumps?
            if arity = 1 [ continue ]

            either block? params/2 [
                either block? params/2/1 [
                    params/2: reduce ['ind params/2/1/1]
                ] [
                    params/2: reduce ['mem params/2/1]
                ]
            ] [
                throw reduce ['immidiate-dst opcode]
            ]
        ]

        ;; Resolve labels
        labels: #()
        offset: 0

        foreach instr program/instructions [
            if set-word? instr [
                labels/:instr: offset
                continue
            ]

            offset: offset + 1
        ]

        normalize-val: function [val off] [
            ;; Lookup labels
            if word? val [
                lbl: labels/:val
                if none? lbl [
                    throw reduce ['undefined-label val]
                ]
                val: lbl - off
            ]
            ;; Wrap values
            val: val % ARENA-WIDTH
            if val < 0 [
                val: ARENA-WIDTH + val
            ]
            val
        ]

        ;; Assemble
        code: make vector! []
        offset: 0
        foreach instr program/instructions [
            if set-word? instr [ continue ]
            opcode: first instr
            params: next instr

            src: normalize-val params/1/2 offset
            dst: 0
            src-flag: false
            dst-flag: false

            if params/1/1 = 'mem [
                src-flag: true
            ]

            if 2 = length? params [
                dst: normalize-val params/2/2 offset
                if params/2/1 = 'ind [
                    dst-flag: true
                ]
            ]

            if opcode = 'data [ dst: src src: 0 ]

            append code Encoding/encode opcode src-flag dst-flag src dst
            offset: offset + 1
        ]

        unless none? program/entrypoint [
            idx: labels/(to-set-word program/entrypoint)
            if none? idx [
                throw reduce ['undefined-entrypoint program/entrypoint]
            ]
            program/origin: idx
        ]
        program/code: code

        program
    ]

    assemble-file: function [fn] [
        throw 'TODO
    ]
]

print "Termination Game"
sim: Simulator/make-simulator
program-a: Assembler/assemble-block [
    name "Imp"

    move [0] [1]
]

program-b: Assembler/assemble-block [
    name "Bomber"

start:
   add 4 [counter]
   move [counter] [[counter]]
   jump start
counter: data 2
]

Simulator/init sim program-a program-b
loop 8192 [
    Simulator/step sim
    if sim/status <> 'tie [
        break
    ]
]
probe sim
switch sim/status [
    tie [ print "Tie!" ]
    a-wins [ print "Process A wins!" ]
    b-wins [ print "Process B wins!" ]
]
prin "At cycle " print sim/cycle
